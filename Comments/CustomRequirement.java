package Comments;

import combat.Combat;
import characters.Character;

public interface CustomRequirement {
	boolean meets(Combat c, Character self, Character other);
}
