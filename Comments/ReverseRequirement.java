package Comments;

import java.util.List;

import characters.Character;
import combat.Combat;

public class ReverseRequirement implements CustomRequirement {
	private List<CustomRequirement> reqs;
	
	public ReverseRequirement(List<CustomRequirement> reqs){
		this.reqs = reqs;
	}
	@Override
	public boolean meets(Combat c, Character self, Character other) {
		for(CustomRequirement r : reqs){
			if(!r.meets(c, other, self)){
				return false;
			}
		}
		return true;
	}

}
