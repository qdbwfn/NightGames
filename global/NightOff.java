package global;

import java.util.HashMap;

import characters.Player;
import daytime.Scene;

public class NightOff implements Scene {
	private Player player;
	public NightOff(Player player){
		this.player=player;
		Global.current=this;
		play("Choices");		
	}
	public void offerChoices(){
		Global.gui().choose("Stay in");
	}
	@Override
	public void respond(String response) {
		if(response.startsWith("Stay in")){
			Global.gui().endMatch();
		}

	}
	@Override
	public boolean play(String response) {
		if(response.startsWith("Choices")){
			offerChoices();
		}
		return false;
	}
	@Override
	public String morning() {
		return "";
	}
	@Override
	public String mandatory() {
		return "";
	}
	@Override
	public void addAvailable(HashMap<String, Integer> available) {
		// TODO Auto-generated method stub
		
	}

}
