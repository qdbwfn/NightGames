package actions;

import items.Flask;
import items.Item;
import items.Potion;
import status.Buzzed;
import status.Oiled;
import global.Global;
import global.Modifier;

import characters.Character;

public class Use extends Action {
	private Item item;
	
	public Use(Item item) {	
		super("Use "+item.getName());
		if(item==Flask.Lubricant){
			name="Oil up";
		}
		else if(item==Potion.EnergyDrink){
			name="Energy Drink";
		}
		else if(item==Potion.Beer){
			name="Beer";
		}
		this.item=item;
	}

	@Override
	public boolean usable(Character user) {
		return user.has(item)&&(!user.human()||Global.getMatch().condition!=Modifier.noitems);
	}

	@Override
	public Movement execute(Character user) {
		if(item==Flask.Lubricant){
			if(user.human()){
				Global.gui().message("You cover yourself in slick oil. It's a weird feeling, but it should make it easier to escape from a hold.");
			}
			user.add(new Oiled(user));
			user.consume(Flask.Lubricant, 1);
			return Movement.oil;
		}
		else if(item==Potion.EnergyDrink){
			if(user.human()){
				Global.gui().message("You chug down the unpleasant drink. Your tiredness immediately starts to recede.");
			}		
			user.heal(10+Global.random(10));
			user.consume(Potion.EnergyDrink, 1);
			return Movement.energydrink;
		}
		else if(item==Potion.Beer){
			if(user.human()){
				Global.gui().message("You pop open a beer and chug it down, feeling buzzed and a bit slugish.");
			}		
			user.add(new Buzzed(user));
			user.consume(Potion.Beer, 1);
			return Movement.beer;
		}
		return Movement.wait;
	}

	@Override
	public Movement consider() {
		if(item==Flask.Lubricant){
			return Movement.oil;
		}
		else if(item==Potion.EnergyDrink){
			return Movement.energydrink;
		}
		else if(item==Potion.Beer){
			return Movement.beer;
		}
		return Movement.wait;
	}

}
