package actions;

import items.Item;
import items.Trophy;

import java.io.PrintWriter;
import java.io.StringWriter;

import areas.Area;
import global.Flag;
import global.Global;
import gui.GUI;
import characters.Character;
import characters.Trait;

public class Locate extends Action {
	private static final long serialVersionUID = 1L;

	private boolean done;

	public Locate() {
		super("Locate");
		done = false;
	}

	@Override
	public boolean usable(Character self) {
		boolean hasUnderwear = false;
		for (Item i : self.getInventory())
			if (i.getClass()==Trophy.class)
				hasUnderwear = true;
		return false;
	}

	@Override
	public Movement execute(Character self) {
		GUI gui = Global.gui();
		gui.clearCommand();
		gui.clearText();
		gui.validate();
		gui.message("Thinking back to your 'games' with Reyka,"
				+ " you take out one of your trophies so you can"
				+ " find its previous owner:");
		handleEvent(self, "Start");
		return Movement.locating;
	}

	public void handleEvent(Character self, String choice) {
		Character target;
		GUI gui = Global.gui();
		if (choice.equals("Start")) {
			if (self.has(Trophy.AngelTrophy))
				gui.choose(this, "Angel", self);
			if (self.has(Trophy.CassieTrophy))
				gui.choose(this, "Cassie", self);
			if (self.has(Trophy.JewelTrophy))
				gui.choose(this, "Jewel", self);
			if (self.has(Trophy.MaraTrophy))
				gui.choose(this, "Mara", self);
			if (Global.checkFlag(Flag.Reyka) && self.has(Trophy.ReykaTrophy))
				gui.choose(this, "Reyka", self);
			if (Global.checkFlag(Flag.Eve) && self.has(Trophy.EveTrophy))
				gui.choose(this, "Eve", self);
			if (Global.checkFlag(Flag.Samantha) && self.has(Trophy.SamanthaTrophy))
				gui.choose(this, "Samantha", self);
			if (Global.checkFlag(Flag.Yui) && self.has(Trophy.YuiTrophy))
				gui.choose(this, "Yui", self);
			if (self.has(Trophy.MayaTrophy))
				gui.choose(this, "Maya",self);
		} else if ((target = Global.getNPC(choice)) != null) {
			Item sought = target.getUnderwear();

			if (sought == null) {
				StringWriter writer = new StringWriter();
				new UnsupportedOperationException()
						.printStackTrace(new PrintWriter(writer));
				gui.clearText();
				gui.message("If you see this text ingame, something went wrong with"
						+ " the locator function. Please take the time to send the information"
						+ " below to The Silver Bard at his wordpress blog or Fenoxo's Forum: "
						+ "\n\nSelf: "
						+ self.name()
						+ "("
						+ self.human()
						+ ")\n"
						+ "Choice: "
						+ choice
						+ "\nStacktrace:\n"
						+ writer.toString());
				gui.clearCommand();
				gui.choose(this, "Leave", self);
			}
			String desc = sought.getName();
			if (self.has(sought)) {
				Area area = target.location();
				gui.clearText();
				gui.message("Focusing on the essence contained in the "
						+ desc
						+ ". In your mind, an image of the "
						+ area.name
						+ " appears. It falls apart as quickly as it came to be, but you know where "
						+target.name()+" currently is. Your hard-earned trophy is already burning up in those creepy "
						+"purple flames, the smoke flowing from your nose straight to your crotch and setting another fire there.");
				self.tempt(15);
				self.consume(sought, 1);
				gui.clearCommand();
				gui.choose(this, "Leave", self);
			} else {
				gui.clearText();
				gui.message("You need some of "
						+ target.name()
						+ "'s personal belongings to find her. Underwear would work.");
				execute(self);
			}
		} else if (choice.equals("Leave")) {
			gui.clearText();
			Global.getMatch().resume();
		} else {
			StringWriter writer = new StringWriter();
			new UnsupportedOperationException()
					.printStackTrace(new PrintWriter(writer));
			gui.clearText();
			gui.message("If you see this text in game, something went wrong with"
					+ " the locator function. Please take the time to send the information"
					+ " below to The Silver Bard at his wordpress blog or Fenoxo's Forum: "
					+ "\n\nSelf: "
					+ self.name()
					+ "("
					+ self.human()
					+ ")\n"
					+ "Choice: "
					+ choice
					+ "\nStacktrace:\n"
					+ writer.toString());
			gui.clearCommand();
			gui.choose(this, "Leave", self);
		}
	}

	@Override
	public Movement consider() {
		return Movement.locating;
	}
	public boolean freeAction(){
		return true;
	}
}
