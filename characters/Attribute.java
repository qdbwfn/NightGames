package characters;

public enum Attribute {
	Power("Power is primarily used to control and subdue opponents. Power skills mostly reduce the target's stamina which can stun the opponent."),
	Seduction("Seduction skills are mostly aimed at raising an opponents arousal. Many of them are unusable or less effective if an opponent has clothes on."),
	Cunning("Cunning skills are mostly aimed at gaining an advantage over opponents, from positioning during combat, to traps outside of combat and more. Cunning contributes to strip attempts, evasion, counter attacks, and hiding."),
	Perception("The higher your Perception the more accurately you can view the Mood, Lust, and Stamina level of an opponent. Perception also improves your ability to evade traps and attacks. Conversly it also raises your sensitivity to touch."),
	Speed("Speed increases the chance you will act before your opponent. It also increases your evasion."),
	Arcane("Arcane represents your ability to cast traditional magic spells. Arcane spells are very versitile, but consume significant Mojo."),
	Science("Science skills provide a lot of utility and consume little to no Mojo. Instead they consume Battery power, which can be recharged at the Workshop."),
	Dark("Dark skills allow potent control of sexual corruption. Most Dark skills raise the user's Arousal."),
	Fetish("Fetish skills involving magic manipulating sexual kinks. Most Fetish skills affect both the opponent and the user, and require the user to be somewhat aroused."),
	Animism("Animism derives from a powerful feral spirit. Most Animism skills are upgrades to normal Power attacks, but it also provides a massive boost to your abilities when heavily aroused."),
	Ki("Ki skills are martial arts abilities that consume your Stamina. Ki forms provide powerful buffs, but only one can be active at a time."),
	Ninjutsu("Ninjutsu is an old Japanese art focused on hidden tools and trickery. Ninjutsu skills provide a lot of utility and the Bunshin abilities can do extreme damage with enough Mojo."),
	Submissive("Submissive skills involve putting yourself in a disadvantageous position. Submission allows you to deal more pleasure while being fucked and turn Shame to your advantage."),
	Hypnosis("Hypnosis is a potent specialization that involves mind control. Most Hypnotic skills require the target to be Charmed or Enthralled."),
	Professional("The oldest profession. Most Professional skills become more effective after consecutive use."),
	Temporal("Temporal is a specialization revolving around time travel. Winding up the Temporal Manipulator gives charges that are consumed by powerful skills.");
	
	private String desc;
	private Attribute(String desc){
		this.desc = desc;
	}
	public String getDescription(){
		return desc;
	}
}
