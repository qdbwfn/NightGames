package characters;

import global.Global;

import items.Component;
import items.Item;

import java.util.ArrayList;
import java.util.HashSet;

import actions.Action;
import actions.Move;
import actions.Movement;

import combat.Combat;
import daytime.Daytime;

import skills.Skill;
import skills.Tactics;

public class Decider {
	public static ArrayList<HashSet<Skill>> parseSkills(HashSet<Skill> available, Combat c, NPC character){
		Character target;
		if(c.p1==character){
			target=c.p2;
		}
		else{
			target=c.p1;
		}
		HashSet<Skill> damage = new HashSet<Skill>();
		HashSet<Skill> pleasure = new HashSet<Skill>();
		HashSet<Skill> fucking = new HashSet<Skill>();
		HashSet<Skill> position = new HashSet<Skill>();
		HashSet<Skill> debuff = new HashSet<Skill>();
		HashSet<Skill> recovery = new HashSet<Skill>();
		HashSet<Skill> calming = new HashSet<Skill>();
		HashSet<Skill> summoning = new HashSet<Skill>();
		HashSet<Skill> stripping = new HashSet<Skill>();
		ArrayList<HashSet<Skill>> priority = new ArrayList<HashSet<Skill>>();
		for(Skill a:available){
			if(a.type()==Tactics.damage){
				damage.add(a);
			}
			else if(a.type()==Tactics.pleasure){
				pleasure.add(a);
			}
			else if(a.type()==Tactics.fucking){
				fucking.add(a);
			}
			else if(a.type()==Tactics.positioning){
				position.add(a);
			}
			else if(a.type()==Tactics.debuff){
				debuff.add(a);
			}
			else if(a.type()==Tactics.recovery){
				recovery.add(a);
			}
			else if(a.type()==Tactics.calming){
				calming.add(a);
			}
			else if(a.type()==Tactics.summoning){
				summoning.add(a);
			}
			else if(a.type()==Tactics.stripping){
				stripping.add(a);
			}
		}
		switch(character.mood){
		case confident:
			priority.add(pleasure);
			priority.add(fucking);
			priority.add(position);
			priority.add(stripping);
			priority.add(summoning);
			priority.add(damage);
			priority.add(debuff);
			break;
		case angry:
			priority.add(damage);
			priority.add(debuff);
			priority.add(position);
			priority.add(stripping);
			priority.add(fucking);
			break;
		case nervous:
			priority.add(position);
			priority.add(calming);
			priority.add(summoning);
			priority.add(recovery);
			priority.add(pleasure);
			priority.add(damage);
			break;
		case desperate:
			priority.add(calming);
			priority.add(recovery);
			priority.add(position);
			priority.add(damage);
			priority.add(pleasure);
			break;
		case horny:
			priority.add(fucking);
			priority.add(stripping);
			priority.add(pleasure);
			priority.add(position);
			break;
		case dominant:
			priority.add(fucking);
			priority.add(stripping);
			priority.add(pleasure);
			priority.add(debuff);
			priority.add(summoning);
			priority.add(position);
			priority.add(damage);
			break;
		}
/*	if(character.getArousal().percent()>85||character.getStamina().percent()<10){
			priority.add(recovery);
			priority.add(damage);
			priority.add(pleasure);
		}
		if((c.stance.penetration(character)&&c.stance.dom(character))||c.stance.enumerate()==Stance.sixnine||(c.stance.dom(character)&&c.stance.enumerate()==Stance.behind)){
			priority.add(pleasure);
			priority.add(pleasure);
			priority.add(damage);
			priority.add(recovery);
		}
		if(!target.canAct()){
			priority.add(stripping);
			priority.add(pleasure);
			priority.add(summoning);
			priority.add(position);
		}
		else if(!target.nude()&&(target.getArousal().percent()>60||target.getStamina().percent()<50)){
			priority.add(stripping);
			priority.add(pleasure);
			priority.add(damage);
			priority.add(position);		
		}
		else if(c.stance.dom(character)){
			priority.add(pleasure);
			priority.add(stripping);
			priority.add(summoning);
			priority.add(damage);
			priority.add(position);
			priority.add(debuff);
		}
		else{
			priority.add(summoning);
			priority.add(pleasure);
			priority.add(debuff);
			priority.add(damage);
			priority.add(position);
			priority.add(stripping);
			priority.add(recovery);
		}
*/		return priority;
	}
	
	public static Action parseMoves(HashSet<Action> available,HashSet<Movement> radar,NPC character){
		HashSet<Action> enemy = new HashSet<Action>();
		HashSet<Action> safe = new HashSet<Action>();
		HashSet<Action> utility = new HashSet<Action>();
		HashSet<Action> sneaky = new HashSet<Action>();
		HashSet<Action> highpri = new HashSet<Action>();
		HashSet<Action> lowpri = new HashSet<Action>();
		if(character.nude()){
			for(Action act: available){
				if(act.consider()==Movement.resupply){
					return act;
				}
/*				else if(act.getClass()==Move.class){
					Move movement = (Move)act;
					if(movement.consider()==Movement.union&&!radar.contains(Movement.union)||movement.consider()==Movement.dorm&&!radar.contains(Movement.dorm)){
						return act;
					}
				}*/
			}
		}
		if(character.getArousal().percent()>=40&&!character.location().humanPresent()&&radar.isEmpty()){
			for(Action act: available){
				if(act.consider()==Movement.masturbate){
					return act;
				}
			}
		}
		if(character.getStamina().percent()<=60||character.getArousal().percent()>=30){
			for(Action act: available){
				if(act.consider()==Movement.bathe){
					return act;
				}
/*				else if(act.getClass()==Move.class){
					Move movement = (Move)act;
					if(movement.consider()==Movement.pool||movement.consider()==Movement.shower){
						return act;
					}
				}*/
			}
		}
		if(character.get(Attribute.Science)>=1&&!character.has(Component.Battery,10)){
			for(Action act: available){
				if(act.consider()==Movement.recharge){
					return act;
				}
			}
/*			Move path = character.findPath(Global.getMatch().gps("Workshop"));
			if( path!=null){
				return path;
			}*/
		}
		if(character.location==character.goal){
			character.plan = character.rethink();
		}
		for(Action act: available){
			if(radar.contains(act.consider())){
				enemy.add(act);
			}
			else if(act.consider()==Movement.bathe||act.consider()==Movement.craft||act.consider()==Movement.scavenge||act.consider()==Movement.hide||act.consider()==Movement.trap||act.consider()==Movement.wait||act.consider()==Movement.engineering||act.consider()==Movement.dining){
				utility.add(act);
			}
			else{
				safe.add(act);
			}
		}
		if(character.plan == Emotion.hunting&&!enemy.isEmpty()){
			highpri.addAll(enemy);
		}
		if(!character.location().humanPresent()){
			highpri.addAll(utility);
		}

		else{
			highpri.addAll(safe);
		}
		if(highpri.isEmpty()){
			highpri.addAll(available);
		}
		Action[] actions = highpri.toArray(new Action[highpri.size()]);
		return actions[Global.random(actions.length)];
	}
	public static void visit(Character self){
		int max = 0;
		Character bff = null;
		if(!self.attractions.isEmpty()){
			for(Character friend: self.attractions.keySet()){
				if(self.getAttraction(friend)>max&&!friend.human()){
					max = self.getAttraction(friend);
					bff = friend;
				}
			}
			if(bff!=null){
				self.gainAffection(bff, Global.random(3)+1);
				bff.gainAffection(self, Global.random(3)+1);
				switch(Global.random(3)){
				case 0:
					Daytime.train(self, bff, Attribute.Power);
				case 1:
					Daytime.train(self, bff, Attribute.Cunning);
				default:
					Daytime.train(self, bff, Attribute.Seduction);
				}
			}
		}
	}
}
