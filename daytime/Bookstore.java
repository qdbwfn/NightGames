package daytime;

import items.Component;
import items.Consumable;
import items.Item;
import items.Potion;
import global.Flag;
import global.Global;


import characters.Character;

public class Bookstore extends Store {
	public Bookstore(Character player) {
		super("Bookstore", player);
		add(Potion.EnergyDrink);
		add(Consumable.ZipTie);
		add(Component.Phone);
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.basicStores);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		if(choice=="Start"){
			acted = false;
		}
		if(choice=="Leave"){
			done(acted);
			return;
		}
		checkSale(choice);
		if(player.human()){
			Global.gui().message("In addition to textbooks, the campus bookstore sells assorted items for everyday use.");			
			for(Item i: stock.keySet()){
				Global.gui().message(i.getName()+": $"+i.getPrice());
			}
			Global.gui().message("You have: $"+player.money+" available to spend.");
			displayGoods();
			Global.gui().choose(this,"Leave");
		}
	}
	@Override
	public void shop(Character npc, int budget) {
		int remaining = Math.min(budget, 50);
		int bored = 0;
		while(remaining>25&&bored<5){
			for(Item i:stock.keySet()){
				if(remaining>i.getPrice()&&!npc.has(i,10)){
					npc.gain(i);
					npc.money-=i.getPrice();
					remaining-=i.getPrice();
				}
				else{
					bored++;
				}
			}
		}
	}
}
