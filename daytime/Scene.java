package daytime;

import java.util.HashMap;

public interface Scene {
	public void respond(String response);
	public boolean play(String response);
	public String morning();
	public String mandatory();
	public void addAvailable(HashMap<String,Integer> available);
}
