package daytime;

import characters.Attribute;
import characters.Character;
import characters.Mara;
import characters.NPC;
import characters.Yui;
import global.Flag;
import global.Global;
import global.Modifier;
import items.Component;
import items.Consumable;
import items.Flask;

public class YuiTime extends Activity {
	private boolean acted;
	private NPC sprite;
	
	public YuiTime(Character player) {
		super("Yui", player);
		sprite = new Yui().character;
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean known() {
		return Global.checkFlag(Flag.YuiLoyalty);
	}

	@Override
	public void visit(String choice) {
		Global.gui().clearText();
		Global.gui().clearCommand();
		sprite.change(Modifier.normal);
		if(choice=="Start"){
			acted = false;
			if(Global.checkFlag(Flag.YuiUnlocking) && !Global.checkFlag(Flag.Yui)){
				Global.gui().message("You head to Yui's hideout, but she is nowhere to be found. Since she doesn't have a phone, you have no way to contact "
						+ "her. The girl is frustratingly elusive when you actually need to talk to her. No wonder Maya and Aesop were having so much trouble trying "
						+ "to invite her to the Games.");
			}
			if(Global.checkFlag(Flag.YuiAvailable)){
				Global.gui().loadPortrait(player, sprite);
				Global.gui().message("Yui greets you happily when you arrive at her hideout/shed. <i>\"Master! It's good to see you. I've collected some items "
						+ "you may find useful.\"</i> She grins and hands you the items proudly. She's acting so much like an excited puppy that you end up patting "
						+ "her on the head. She blushes, but seems to like it. <i>\"Would you like to spend some time training? Or is there something else I can do "
						+ "to help you?\"</i><br>");
				if(Global.checkFlag(Flag.Yui)){
					Global.gui().message("She gives you a shy, but slightly suggestive look. You're getting as much sex as you could possibly hope for each night, "
							+ "but there's no harm in giving your loyal ninja girl some personal attention.");
				}else{
					Global.gui().message("Hearing the offer from such an attractive blushing girl stirs up a natural temptation. She'd almost certainly agree to "
							+ "any sexual request you could think of. However, knowing Yui's sincerity and "
							+ "innocence, your conscience won't let you take advantage of her loyalty.");
				}
				Global.gui().choose(this,"Train with Yui");
				if(Global.checkFlag(Flag.Yui)){
					Global.getNPC("Yui").gainAffection(player, 1);
					player.gainAffection(Global.getNPC("Yui"), 1);
				}else{
					Global.modCounter(Flag.YuiAffection, 1);
				}
				Global.unflag(Flag.YuiAvailable);
				acted = true;
				player.gain(Component.Tripwire);
				player.gain(Component.Rope);
				if(player.getPure(Attribute.Ninjutsu)>=1){
					player.gain(Consumable.needle,3);
				}
				if(player.getPure(Attribute.Ninjutsu)>=3){
					player.gain(Consumable.smoke,2);
				}
				if(player.getPure(Attribute.Ninjutsu)>=18){
					player.gain(Flask.Sedative,1);
				}
			}else{
				Global.gui().message("You head to Yui's hideout, but find it empty. She must be out doing something. It would be a lot easier to track her down if she "
						+ "had a phone.");
			}
			Global.gui().choose(this,"Leave");
		}
		else if(choice=="Leave"){
			Global.gui().showNone();
			done(acted);
		}
		else if(choice.startsWith("Train")){
			Global.gui().message("Yui's skills at subterfuge turn out to be as strong as she claimed. She's also quite a good teacher. Apparently she helped train her "
					+ "younger sister, so she's used to it. Nothing she teaches you is overtly sexual, but you can see some useful applications for the Games.");
			player.mod(Attribute.Ninjutsu, 1);
			if(Global.checkFlag(Flag.Yui)){
				Global.getNPC("Yui").gainAffection(player, 1);
				player.gainAffection(Global.getNPC("Yui"), 1);
			}else{
				Global.modCounter(Flag.YuiAffection, 1);
			}
			Global.gui().choose(this,"Leave");
		}
	}

	@Override
	public void shop(Character npc, int budget) {
		npc.gain(Consumable.needle,3);
		npc.gain(Consumable.smoke,2);
		npc.gain(Flask.Sedative,1);

	}

}
