package skills;

import items.Item;
import items.Toy;
import global.Global;
import global.Modifier;
import combat.Combat;
import combat.Result;

import characters.Attribute;
import characters.Character;
import characters.Anatomy;

public class UseDildo extends Skill{

	public UseDildo(Character self) {
		super(Toy.Dildo.getName(), self);
	}

	@Override
	public boolean requirements() {
		return true;
	}

	@Override
	public boolean requirements(Character user) {
		return true;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return (self.has(Toy.Dildo)||self.has(Toy.Dildo2))&&self.canAct()&&target.hasPussy()&&c.stance.reachBottom(self)&&target.pantsless()&&!c.stance.penetration(self)
				&&(!self.human()||Global.getMatch().condition!=Modifier.notoys);
	}

	@Override
	public void resolve(Combat c, Character target) {
		int m = 0;
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(self.has(Toy.Dildo2)){
				if(self.human()){
					c.write(self,deal(0,Result.upgrade,target));
				}
				else if(target.human()){
					
				}
				m = 5+Global.random(15)+target.get(Attribute.Perception)+(self.get(Attribute.Science)/2);
				m = self.bonusProficiency(Anatomy.toy, m);
				target.pleasure(m,Anatomy.genitals,c);
			}
			else{
				if(self.human()){
					c.write(self,deal(0,Result.normal,target));
				}
				else if(target.human()){
					
				}
				m = Global.random(10)+target.get(Attribute.Perception)+(self.get(Attribute.Science)/2);
				m = self.bonusProficiency(Anatomy.toy, m);
				target.pleasure(m,Anatomy.genitals,c);
			}			
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public Skill copy(Character user) {
		return new UseDildo(user);
	}

	@Override
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You try to slip a dildo into "+target.name()+", but she blocks it.";
		}
		else if(modifier == Result.upgrade){
			return "You touch the imperceptively vibrating dildo to "+target.name()+"'s love button and she jumps as if shocked. Before she can defend herself, you " +
					"slip it into her pussy. She starts moaning in pleasure immediately.";
		}
		else{
			return "You rub the dildo against "+target.name()+"'s lower lips to lubricate it before you thrust it inside her. She can't help moaning a little as you " +
					"pump the rubber toy in and out of her pussy.";
		}
				
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return null;
	}

	@Override
	public String describe() {
		return "Pleasure opponent with your dildo";
	}

}
