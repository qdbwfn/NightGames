package skills;

import status.Shamed;
import status.Stsflag;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Spank extends Skill {

	public Spank(Character self) {
		super("Spank", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.pantsless()&&!c.stance.prone(target)&&c.stance.reachBottom(self)&&self.canAct();
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(self.has(Trait.disciplinarian)&&!c.stance.penetration(self)){
			if(self.human()){
				c.write(self,deal(0,Result.special,target));
			}
			else if(target.human()){			
				c.write(self,receive(0,Result.special,target));
			}
			if(Global.random(10)>=5||!target.is(Stsflag.shamed)&&self.canSpend(5)){
				target.add(new Shamed(target),c);
				self.spendMojo(5);
				target.emote(Emotion.angry,10);
				target.emote(Emotion.nervous,15);
			}
			target.pain(Global.random(6+target.get(Attribute.Perception)/2)+3,Anatomy.genitals,c);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.normal,target));
			}
			else if(target.human()){			
				c.write(self,receive(0,Result.normal,target));
			}
			target.pain(Global.random(6)+3,Anatomy.ass,c);
		}
		target.emote(Emotion.angry,45);
		target.emote(Emotion.nervous,15);
		self.buildMojo(10);
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Seduction)>=8;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Seduction)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new Spank(user);
	}
	public int speed(){
		return 8;
	}
	public int accuracy(){
		return 4;
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return "You try to spank "+target.name()+", but she dodges away.";
		}
		if(modifier==Result.special){
			return "You bend "+target.name()+" over your knee and spank her, alternating between hitting her soft butt cheek and her sensitive pussy.";
		}
		else{
			return "You spank "+target.name()+" on her naked butt cheek.";
		}
		
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier==Result.miss){
			return self.name()+" aims a slap at your ass, but you dodge it.";
		}
		if(modifier==Result.special){
			return self.name()+" bends you over like a misbehaving child and spanks your ass twice. The third spank aims lower and connects solidly with your ballsack, " +
					"injuring your manhood along with your pride.";
		}
		else{
			return self.name()+" lands a stinging slap on your bare ass.";
		}
		
	}

	@Override
	public String describe() {
		return "Slap opponent on the ass";
	}
}
