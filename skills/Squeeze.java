package skills;

import items.Component;
import items.Item;
import items.Toy;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Anatomy;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Squeeze extends Skill {

	public Squeeze(Character self) {
		super("Squeeze Balls", self);
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.hasBalls()&&c.stance.reachBottom(self)&&self.canAct()&&!self.has(Trait.shy);
	}

	@Override
	public void resolve(Combat c, Character target) {
		if(target.roll(this, c, accuracy()+self.tohit())){
			if(target.pantsless()){
				if(self.has(Toy.ShockGlove)&&self.has(Component.Battery,2)){
					self.consume(Component.Battery, 2);
					if(target.human()){
						c.write(self,receive(0,Result.special,target));
					}
					else if(self.human()){
						c.write(self,deal(0,Result.special,target));
					}
					target.pain(Global.random(8+target.get(Attribute.Perception))+8,Anatomy.genitals,c);
				}
				else if(target.has(Trait.armored)){
					if(target.human()){
						c.write(self,receive(0,Result.item,target));
					}
					else if(self.human()){
						c.write(self,deal(0,Result.item,target));
					}
					target.pain(Global.random(7)+5,Anatomy.genitals,c);
				}
				else{
					if(target.human()){
						c.write(self,receive(0,Result.normal,target));
					}
					else if(self.human()){
						c.write(self,deal(0,Result.normal,target));
					}
					target.pain(Global.random(7)+5,Anatomy.genitals,c);
				}
			}
			else{
				if(target.human()){
					c.write(self,receive(0,Result.weak,target));
				}
				else if(self.human()){
					c.write(self,deal(0,Result.weak,target));
				}
				target.pain(Global.random(7)+5-(2*target.bottom.size()),Anatomy.genitals,c);
			}
			if(self.has(Trait.wrassler)){
				target.calm(3,c);
			}
			else{
				target.calm(8,c);
			}
			self.buildMojo(10);
			target.emote(Emotion.angry,15);
		}
		else{
			if(self.human()){
				c.write(self,deal(0,Result.miss,target));
			}
			else if(target.human()){
				c.write(self,receive(0,Result.miss,target));
			}
		}
	}

	@Override
	public boolean requirements() {
		return self.getPure(Attribute.Power)>=9;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Power)>=9;
	}

	@Override
	public Skill copy(Character user) {
		return new Squeeze(user);
	}
	public Tactics type() {
		return Tactics.damage;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return "You try to grab "+target.name()+"'s balls, but she avoids it.";
		}
		else if(modifier == Result.special){
			return "You use your shock glove to deliver a painful jolt directly into "+target.name()+"'s testicles.";
		}
		else if(modifier == Result.weak){
			return "You grab the bulge in "+target.name()+"'s "+target.bottom.peek()+" and squeeze.";
		}
		else if(modifier == Result.item){
			return "You grab the bulge in "+target.name()+"'s "+target.bottom.peek()+", but find it solidly protected.";
		}
		else{
			return "You manage to grab "+target.name()+"'s balls and squeeze them hard. You feel a twinge of empathy when she cries out in pain, but you maintain your grip.";
		}
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		if(modifier == Result.miss){
			return self.name()+" grabs at your balls, but misses.";
		}
		else if(modifier == Result.special){
			return self.name()+" grabs your naked balls roughly in her gloved hand. A painful jolt of electricity shoots through your groin, sapping your will to fight.";
		}
		else if(modifier == Result.weak){
			return self.name()+" grabs your balls through your "+target.bottom.peek()+" and squeezes hard.";
		}
		else if(modifier == Result.item){
			return self.name()+" grabs your crotch through your "+target.bottom.peek()+", but you can barely feel it.";
		}
		else{
			return self.name()+" reaches between your legs and grabs your exposed balls. You writhe in pain as she pulls and squeezes them.";
		}
	}
	public String toString(){
		if(self.has(Toy.ShockGlove)){
			return "Shock Balls";
		}
		else{
			return name;
		}
	}

	@Override
	public String describe() {
		return "Grab opponent's groin, deals more damage if naked";
	}
}
