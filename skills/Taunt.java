package skills;

import status.Shamed;
import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

import combat.Combat;
import combat.Result;

public class Taunt extends Skill {

	public Taunt(Character self) {
		super("Taunt", self);
	}

	@Override
	public boolean requirements() {
		
		return self.getPure(Attribute.Cunning)>=8;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return target.nude()&&!c.stance.sub(self)&&self.canSpend(5)&&self.canAct()&&!self.has(Trait.shy);
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.spendMojo(5);
		if(self.human()){
			c.write(self,deal(0,Result.normal,target));
		}
		else if(target.human()){
			c.write(self,receive(0,Result.normal,target));
		}
		target.tempt(3+self.get(Attribute.Seduction)/(Math.max(1,4-self.getSkimpiness())),c);
		if(Global.random(4)>=2){
			target.add(new Shamed(target),c);
		}
		target.emote(Emotion.angry,30);
		target.emote(Emotion.nervous,15);
		self.emote(Emotion.dominant, 20);
		target.modMojo(-10);
		
	}

	@Override
	public boolean requirements(Character user) {
		return user.getPure(Attribute.Cunning)>=8;
	}

	@Override
	public Skill copy(Character user) {
		return new Taunt(user);
	}
	public int speed(){
		return 9;
	}
	public Tactics type() {
		return Tactics.pleasure;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You tell "+target.name()+" that if she's so eager to be fucked senseless, you're available during off hours.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.taunt();
	}

	@Override
	public String describe() {
		return "Embarrass your opponent, may inflict Shamed";
	}
}
