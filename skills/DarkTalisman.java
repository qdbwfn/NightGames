package skills;

import items.Consumable;
import status.Enthralled;
import status.Stsflag;
import characters.Character;

import combat.Combat;
import combat.Result;

public class DarkTalisman extends Skill {

	public DarkTalisman(Character self) {
		super("Dark Talisman", self);
	}

	@Override
	public boolean requirements() {
		return self.getRank()>=1;
	}

	@Override
	public boolean requirements(Character user) {
		return user.getRank()>=1;
	}

	@Override
	public boolean usable(Combat c, Character target) {
		return self.canAct()&&c.stance.mobile(self)&&!c.stance.prone(self)&&!target.is(Stsflag.enthralled);
	}

	@Override
	public String describe() {
		return "Consume the mysterious talisman to control your opponent";
	}

	@Override
	public void resolve(Combat c, Character target) {
		self.consume(Consumable.Talisman, 1);
		if(self.human()){
			c.write(deal(0,Result.normal,target));
		}else if(target.human()){
			c.write(receive(0,Result.normal,target));
		}
		target.add(new Enthralled(target),c);
	}

	@Override
	public Skill copy(Character user) {
		return new DarkTalisman(user);
	}

	@Override
	public Tactics type() {
		return Tactics.debuff;
	}

	@Override
	public String deal(int damage, Result modifier, Character target) {
		return "You brandish the dark talisman, which seems to glow with power. The trinket crumbles to dust, but you see the image remain in the reflection of "+target.name()+"'s eyes.";
	}

	@Override
	public String receive(int damage, Result modifier, Character target) {
		return self.name()+" holds up a strange talisman. You feel compelled to look at the thing, captivated by its unholy nature.";
	}

}
