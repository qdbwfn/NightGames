package stance;

import combat.Combat;

import characters.Character;
import characters.Anatomy;
import characters.Trait;

public class Flying extends Position {
	private static final long serialVersionUID = 1953597655795344915L;

	public Flying (Character succ, Character target) {
		super(succ, target, Stance.flying);
	}
	
	@Override
	public String describe() {
		return "You are flying some twenty feet up in the air,"
				+ " joinned to your partner by your hips.";
	}

	@Override
	public boolean mobile(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean kiss(Character c) {
		return true;
	}

	@Override
	public boolean dom(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean sub(Character c) {
		return !top.equals(c);
	}

	@Override
	public boolean reachTop(Character c) {
		return true;
	}

	@Override
	public boolean reachBottom(Character c) {
		return top.equals(c);
	}

	@Override
	public boolean prone(Character c) {
		return !top.equals(c);
	}

	@Override
	public boolean feet(Character c) {
		return false;
	}

	@Override
	public boolean oral(Character c) {
		return false;
	}

	@Override
	public boolean behind(Character c) {
		return false;
	}

	@Override
	public boolean penetration(Character c) {
		return true;
	}


	public boolean flying(Character c) {
		return true;
	}
	public void decay(){
		time++;
		top.weaken(3);
	}
	public void checkOngoing(Combat c){
		if(top.getStamina().get()<5){
			if(top.human()){
				c.write("You're too tired to stay in the air. You plummet to the ground and "+bottom.name()+" drops on you heavily, knocking the wind out of you.");
			}
			else{
				c.write(top.name()+" falls to the ground and so do you. Fortunately, her body cushions your fall, but you're not sure she appreciates that as much as you do.");
			}
			top.pain(5,Anatomy.chest,c);
			c.stance = new Mount(bottom,top);
		}
	}
	
	@Override
	public Position insert(Character c) {
		if (c.has(Trait.succubus))
			return new Mount(top, bottom);
		else {
			c.weaken(10);
			return new StandingOver(top, bottom);
		}
	}

}
