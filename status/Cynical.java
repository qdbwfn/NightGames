package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Cynical extends Status {
	
	public Cynical(){
		super("Cynical");
	}
	public Cynical(Character affected) {
		super("Cynical", affected);
		flag(Stsflag.cynical);
		if(affected.has(Trait.PersonalInertia)){
			duration = 5;
		}else{
			duration = 3;
		}
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're feeling more cynical than usual and won't fall for any mind games.";
		}
		else{
			return affected.name()+" has a cynical edge in her eyes.";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}
	@Override
	public int damage(int x) {
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return -x/4;
	}

	@Override
	public int evade() {
		return 0;
	}

	@Override
	public int escape() {
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		return -x;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}
	@Override
	public int counter() {
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Cynical(target);
	}
	@Override
	public void turn(Combat c) {
		decay();
	}
}
