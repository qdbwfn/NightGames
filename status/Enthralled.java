package status;

import combat.Combat;

import global.Global;
import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.State;

public class Enthralled extends Status {
	public Character master;
	
	public Enthralled(Character master){
		super("Enthralled");
		this.master=master;
	}
	public Enthralled(Character self,Character master) {
		
		super("Enthralled", self);
		duration = Global.random(3) + (self.state == State.combat ? 1 : 3);
		this.master = master;
		flag(Stsflag.enthralled);
	}
	
	@Override
	public String describe() {
		if(affected.human())
		  return "You feel a constant pull on your mind, forcing you to obey"
				+ " your master's every command.";
		else{
			return affected.name()+" looks dazed and compliant, ready to follow your orders.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if (a == Attribute.Perception)
			return -5;
		return -2;
	}

	@Override
	public int regen() {
		duration--;
		affected.spendMojo(5);
		if (duration <= 0|| affected.check(Attribute.Cunning, 10+5*duration)) {
			affected.removelist.add(this);
			if (affected.human() && affected.state != State.combat)
				Global.gui().message("Everything around you suddenly seems much clearer,"
						+ " like a lens snapped into focus. You don't really remember why"
						+ " you were heading in the direction you where...");
		}
		affected.emote(Emotion.horny,15);
		return 0;
	}

	@Override
	public int damage(int paramInt) {
		return 0;
	}

	@Override
	public int pleasure(int paramInt) {
		return paramInt/4;
	}

	@Override
	public int weakened(int paramInt) {
		return 0;
	}

	@Override
	public int tempted(int paramInt) {
		return paramInt/4;
	}

	@Override
	public int evade() {
		return -20;
	}

	@Override
	public int escape() {
		return -20;
	}

	@Override
	public int gainmojo(int paramInt) {
		return -paramInt;
	}

	@Override
	public int spendmojo(int paramInt) {
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Enthralled(target,master);
	}
	@Override
	public void turn(Combat c) {
		// TODO Auto-generated method stub
		
	}

}
