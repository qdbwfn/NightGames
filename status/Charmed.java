package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Charmed extends Status {
	public Charmed(){
		super("Charmed");
	}
	public Charmed(Character affected) {
		super("Charmed", affected);
		if(affected.has(Trait.PersonalInertia)){
			duration = 3;
		}else{
			duration = 2;
		}
		flag(Stsflag.charmed);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You know that you should be fighting back, but it's so much easier to just surrender.";
		}
		else{
			return affected.name()+" is flush with desire and doesn't seem interested in fighting back.";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}
	@Override
	public int damage(int x) {
		affected.removelist.add(this);
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 3;
	}

	@Override
	public int evade() {
		return 0;
	}

	@Override
	public int escape() {
		return -10;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}
	@Override
	public int counter() {
		return -10;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Charmed(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.horny,15);
		decay();
	}
}
