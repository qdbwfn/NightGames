package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Trait;

public class Drowsy extends Status {
	
	public Drowsy() {
		super("Drowsy");
		magnitude = 1;
		lingering = true;
		stacking = true;
	}
	public Drowsy(Character affected) {
		super("Drowsy",affected);
		if(affected.has(Trait.PersonalInertia)){
			this.duration=6;
		}else{
			this.duration = 4;
		}
		flag(Stsflag.drowsy);
	}
	
	@Override
	public String describe() {
		if(affected.human()){
			return "You feel lethargic and sluggish. You're struggling to remain standing";
		}
		else{
			return affected.name()+" looks extremely sleepy.";
		}
	}

	@Override
	public int mod(Attribute a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int regen() {
		return -3*magnitude;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return -(x*magnitude)/4;
	}

	@Override
	public int weakened(int x) {
		return (x*magnitude)/4;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Status copy(Character target) {
		return new Drowsy(target);
	}
	@Override
	public void turn(Combat c) {
		affected.buildMojo(-5*magnitude);
		decay();
	}

}
