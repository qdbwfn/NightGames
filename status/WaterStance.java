package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class WaterStance extends Status {
	
	public WaterStance(){
		super("Water Form");
	}
	public WaterStance(Character affected) {
		super("Water Form", affected);
		flag(Stsflag.form);
		if(affected.has(Trait.PersonalInertia)){
			this.duration=15;
		}
		else{
			this.duration=10;
		}
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're as smooth and responsive as flowing water.";
		}
		else{
			return affected.name()+" continues her flowing movements.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(Attribute.Power==a){
			return -2;
		}
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new WaterStance(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		decay();
	}

}
