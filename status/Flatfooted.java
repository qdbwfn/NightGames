package status;

import java.util.HashSet;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;

public class Flatfooted extends Status {
	public Flatfooted(int duration){
		super("Flat-Footed");
		flag(Stsflag.distracted);
		this.duration = duration;
	}
	public Flatfooted(Character affected, int duration) {
		super("Flat-Footed", affected);
		flag(Stsflag.distracted);
		this.duration = duration;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You are caught off-guard.";
		}
		else{
			return affected.name()+" is flat-footed and not ready to fight.";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 0;
	}

	@Override
	public int evade() {
		return -10;
	}

	@Override
	public int escape() {
		return -20;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}
	@Override
	public int counter() {
		return -3;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Flatfooted(target,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,5);
		decay();
	}
}
