package status;

import characters.Anatomy;
import characters.Character;
import combat.Combat;

public class ProfMod extends Status {
	private Anatomy part;
	
	public ProfMod(String name, Character affected, Anatomy part, int percent) {
		super(name, affected);
		this.magnitude = percent;
		this.part = part;
		stacking=true;
	}

	@Override
	public String describe() {
		// TODO Auto-generated method stub
		return null;
	}
	public float proficiency(Anatomy using){
		if(using==part){
			return 1.0f+(magnitude/100f);
		}
		return 1.0f;
	}

	@Override
	public void turn(Combat c) {
		// TODO Auto-generated method stub

	}

	@Override
	public Status copy(Character target) {
		// TODO Auto-generated method stub
		return null;
	}

}
