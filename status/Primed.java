package status;

import characters.Character;
import combat.Combat;

public class Primed extends Status {

	public Primed(Character affected, int charges) {
		super("Primed", affected);
		magnitude = charges;
		stacking = true;
		duration = 99;
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You have "+magnitude+" time charges primed.";
		}else{
			return "";
		}
	}

	@Override
	public void turn(Combat c) {
		
	}

	public void decay(){
		return;
	}
	@Override
	public Status copy(Character target) {
		Status copy = new Primed(target,magnitude);
		return copy;
	}

}
