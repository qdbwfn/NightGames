package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;

public class Braced extends Status {
	
	public Braced(){
		super("Braced");
	}
	public Braced(Character affected) {
		super("Braced", affected);
		duration=4;
		flag(Stsflag.braced);
	}

	@Override
	public String describe() {
		return "";
	}

	@Override
	public int mod(Attribute a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 10+(15*duration);
	}

	@Override
	public Status copy(Character target) {
		return new Braced(target);
	}
	@Override
	public void turn(Combat c) {
		decay();
	}

}
