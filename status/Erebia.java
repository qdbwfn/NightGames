package status;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import combat.Combat;

public class Erebia extends Status {

	public Erebia(){
		super("Mana Fortification");
	}
	public Erebia(Character affected, int duration) {
		super("Mana Fortification", affected);
		this.magnitude = affected.get(Attribute.Arcane)/3;
		this.duration = duration;
		flag(Stsflag.erebia);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Magic is still flowing through your body, giving you tremendous speed and power";
		}
		else{
			return affected.name()+" is glowing with powerful magic.";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a==Attribute.Power || a==Attribute.Seduction){
			return magnitude;
		}else if(a==Attribute.Speed){
			return magnitude/2;
		}
		return 0;
	}

	@Override
	public int regen() {
		return 3;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return magnitude;
	}

	@Override
	public Status copy(Character target) {
		return new Erebia(target,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,25);
		affected.emote(Emotion.dominant,25);
		decay();
	}

}
