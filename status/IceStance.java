package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class IceStance extends Status {
	
	public IceStance(){
		super("Ice Form");
	}
	public IceStance(Character affected) {
		super("Ice Form", affected);
		if(affected.has(Trait.PersonalInertia)){
			duration = 15;
		}else{
			duration = 10;
		}
		flag(Stsflag.form);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "You're as frigid as a glacier";
		}
		else{
			return affected.name()+" is cool as ice.";
		}
	}

	@Override
	public int mod(Attribute a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return -x/5;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return -x;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new IceStance(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.confident,5);
		affected.emote(Emotion.dominant,5);
		decay();
	}

}
