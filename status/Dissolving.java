package status;

import combat.Combat;

import items.Clothing;
import global.Global;
import characters.Attribute;
import characters.Character;

public class Dissolving extends Status {
	public Dissolving(){
		super("Dissolving");
	}
	public Dissolving(Character affected) {
		super("Dissolving",affected);
		duration = 2;
		flag(Stsflag.dissolving);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "The corrosive solution continues to eat away at your clothes.";
		}else{
			return affected.name()+"'s clothes are rapidly deteriorating.";
		}
		
	}

	@Override
	public int mod(Attribute a) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int pleasure(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int weakened(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int tempted(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int evade() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int escape() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int counter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Status copy(Character target){ 
		return new Dissolving(target);
	}
	@Override
	public void turn(Combat c) {
		Clothing lost;
		if(affected.nude()){
			affected.removelist.add(this);
			return;
		}
		lost = affected.shredRandom();
		if(lost!=null){
			if(affected.human()){
				c.write("Your "+lost.getName()+" dissolves away.");
			}else{
				c.write("Her "+lost.getName()+" dissolves away.");
			}
		}
		if(affected.nude()){
			affected.removelist.add(this);
			return;
		}
		decay();
	}

}
