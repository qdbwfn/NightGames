package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Horny extends Status {
	
	public Horny(int magnitude, int duration){
		super("Horny");
		this.duration=duration;
		this.magnitude = magnitude;
		lingering = true;
		stacking = true;
	}
	public Horny(Character affected, int magnitude, int duration) {
		super("Horny", affected);
		this.magnitude = magnitude;
		if(affected.has(Trait.PersonalInertia)){
			this.duration=3*duration/2;
		}
		else{
			this.duration=duration;
		}
		lingering = true;
		stacking = true;
		flag(Stsflag.horny);
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your heart pounds in your chest as you try to surpress your arousal.";
		}
		else{
			return affected.name()+" is flushed and her nipples are noticeably hard.";
		}
	}

	@Override
	public int mod(Attribute a) {
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return 0;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 0;
	}

	@Override
	public int evade() {
		return 0;
	}

	@Override
	public int escape() {
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}

	@Override
	public int counter() {
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Horny(target,magnitude,duration);
	}
	@Override
	public void turn(Combat c) {
		affected.tempt(magnitude);
		affected.emote(Emotion.horny,20);
		decay();
	}
}
