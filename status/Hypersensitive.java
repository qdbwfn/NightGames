package status;

import combat.Combat;

import characters.Attribute;
import characters.Character;
import characters.Emotion;
import characters.Trait;

public class Hypersensitive extends Status {
	public Hypersensitive(){
		super("Hypersensitive");
		lingering = true;
	}
	public Hypersensitive(Character affected) {
		super("Hypersensitive", affected);
		flag(Stsflag.hypersensitive);
		if(affected.has(Trait.PersonalInertia)){
			duration = 30;
		}else{
			duration = 20;
		}
	}

	@Override
	public String describe() {
		if(affected.human()){
			return "Your skin tingles and feels extremely sensitive to touch.";
		}
		else{
			return "She shivers from the breeze hitting her skin and has goosebumps";
		}
	}

	@Override
	public int mod(Attribute a) {
		if(a == Attribute.Perception){
			return 4;
		}
		return 0;
	}

	@Override
	public int regen() {
		return 0;
	}

	@Override
	public int damage(int x) {
		return 0;
	}

	@Override
	public int pleasure(int x) {
		return x/3;
	}

	@Override
	public int weakened(int x) {
		return 0;
	}

	@Override
	public int tempted(int x) {
		return 0;
	}

	@Override
	public int evade() {
		return 0;
	}

	@Override
	public int escape() {
		return 0;
	}

	@Override
	public int gainmojo(int x) {
		return 0;
	}

	@Override
	public int spendmojo(int x) {
		return 0;
	}
	@Override
	public int counter() {
		return 0;
	}

	@Override
	public int value() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Status copy(Character target) {
		return new Hypersensitive(target);
	}
	@Override
	public void turn(Combat c) {
		affected.emote(Emotion.nervous,5);
		decay();
	}
}
