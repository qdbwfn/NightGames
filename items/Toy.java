package items;

import characters.Character;

public enum Toy implements Item {
	Dildo		( "Dildo",250,"Big rubber cock: not a weapon","a "	),
	Crop		( "Riding Crop",200,"Delivers a painful sting to instill discipline","a "	),
	Onahole		( "Onahole",300,"An artificial vagina, but you can probably find some real ones pretty easily","an "	),
	Tickler		( "Tickler",300,"Tickles and pleasures your opponent's sensitive areas","a "	),
	Strapon		( "Strap-on Dildo",600,"Penis envy much?","a "),
	Dildo2		( "Sonic Dildo",2000,"Apparently vibrates at the ideal frequency to produce pleasure","a "	),
	Crop2		( "Hunting Crop",1500,"Equipped with the fearsome Treasure Hunter attachment","a "	),
	Onahole2	( "Wet Onahole",3000,"As hot and wet as the real thing","an "	),
	Tickler2	( "Enhanced Tickler",3000,"Coated with a substance that can increase sensitivity","an "	),
	Strapon2	( "Flex-O-Peg",2500,"A more flexible and versatile strapon with a built in vibrator","the patented "),
	ShockGlove	( "Shock Glove",800,"Delivers a safe, but painful electric shock", "a " ),
	Aersolizer	( "Aerosolizer",500, "Turns a liquid into an unavoidable cloud of mist", "an "),
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	private String prefix;
	private int price;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public int getPrice(){
		return price;
	}
	public String getName(){
		return name;
	}
	public String pre(){
		return prefix;
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	private Toy( String name, int price, String desc,String prefix )
	{
		this.name = name;
		this.price = price;
		this.desc = desc;
		this.prefix = prefix;
	}
}
