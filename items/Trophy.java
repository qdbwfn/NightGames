package items;

import characters.Character;

public enum Trophy implements Item {
	CassieTrophy	("Cassie's Panties","Cute and simple panties"),
	MaraTrophy	("Mara's Underwear","She wears boys underwear?"),
	AngelTrophy	("Angel's Thong","There's barely anything here"),
	JewelTrophy	("Jewel's Panties","Surprisingly lacy"),
	ReykaTrophy	("Reyka's Clit Ring","What else can you take from someone who goes commando?"),
	PlayerTrophy("Your Boxers","How did you end up with these?"),
	EveTrophy	("Eve's 'Panties'","Crotchless and of no practical use"),
	KatTrophy	("Kat's Panties","Cute pink panties. Unfortunately there's no cat motif"),
	YuiTrophy	("Yui's Panties","White and innocent, not unlike their owner"),
	MayaTrophy	("Maya's Panties","Black lace. Very sexy"),
	SamanthaTrophy	("Samantha's Thong","A lacy red thong, translucent in all but the most delicate areas."),
	;
	/**
	 * The Item's display name.
	 */
	private String desc;
	private String name;
	/**
	 * @return the Item name
	 */
	public String getDesc()
	{
		return desc;
	}
	public int getPrice(){
		return 0;
	}
	public String getName(){
		return name;
	}
	public String pre(){
		return "";
	}
	@Override
	public void pickup(Character owner) {
		owner.gain(this);
	}
	private Trophy( String name, String desc )
	{
		this.name = name;
		this.desc = desc;
	}
}
