package items;

import characters.Character;

public interface Item extends Loot{
	
	public String getDesc();
	public int getPrice();
	public String getName();
	public String pre();
	@Override
	public void pickup(Character owner);
}
