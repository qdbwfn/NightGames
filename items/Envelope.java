package items;

import characters.Character;
import global.Challenge;

public class Envelope implements Item {
	private Challenge goal;
	public Envelope(Challenge goal){
		this.goal = goal;
	}
	@Override
	public String getDesc() {
		return goal.message();
	}

	@Override
	public int getPrice() {
		return 0;
	}

	@Override
	public String getName() {
		return "Challenge: "+goal.summary();
	}

	@Override
	public String pre() {
		// TODO Auto-generated method stub
		return "a ";
	}

	@Override
	public void pickup(Character owner) {
		// TODO Auto-generated method stub
		
	}

}
