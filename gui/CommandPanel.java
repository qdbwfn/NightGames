package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;

public class CommandPanel extends JPanel{
    private static final List<Character> POSSIBLE_HOTKEYS = Arrays.asList(
                    'q', 'w', 'e', 'r', 't', 'y',
                    'a', 's', 'd', 'f' , 'g', 'h',
                    'z', 'x', 'c', 'v', 'b', 'n'); 
    private static final Set<String> DEFAULT_CHOICES = new HashSet<String>(Arrays.asList("Wait", "Nothing", "Next", "Leave", "Back"));
    private static final int ROW_LIMIT = 6;

//    private JPanel panel;
    private int index;
    private int page;
    private int perPage;
    private Map<Character, KeyableButton> hotkeyMapping;
    private List<KeyableButton> buttons;
    private JPanel rows[];
    public CommandPanel(int width, int height) {
//        panel = new JPanel();
    	super();
        this.setBackground(new Color(245,245,245));
        this.setPreferredSize(new Dimension(width, 150));
        this.setMinimumSize(new Dimension(width, 150));
        this.setBorder(new CompoundBorder());
        hotkeyMapping = new HashMap<Character, KeyableButton>();
        if(height > 720){
        	perPage = 18;
        }else{
        	perPage = 12;
        }
        rows = new JPanel[3];
        rows[0] = new JPanel();
        rows[1] = new JPanel();
        rows[2] = new JPanel();
        for (JPanel row : rows) {
            FlowLayout layout;
            layout = new FlowLayout();
            layout.setVgap(0);
            layout.setHgap(4);
            row.setLayout(layout);
            row.setOpaque(false);
            row.setBorder(BorderFactory.createEmptyBorder());
            this.add(row);
        }
        BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(layout);
        this.setAlignmentY(Component.TOP_ALIGNMENT);
        this.add(Box.createVerticalGlue());
        this.add(Box.createVerticalStrut(15));
        buttons = new ArrayList<KeyableButton>();
        index = 0;
    }

    public JPanel getPanel() {
        return this;
    }

    public void reset() {
        buttons.clear();
        hotkeyMapping.clear();
        clear();
        refresh();
    }

    private void clear() {
        for (JPanel row : rows) {
            row.removeAll();
        }
        for(Character mapping : POSSIBLE_HOTKEYS){
        	hotkeyMapping.remove(mapping);
        }
        index = 0;  
    }

    public void refresh() {
        repaint();
        revalidate();
    }

    public void add(KeyableButton button) {
        page = 0;
        buttons.add(button);
        use(button);
    }

    private void use(KeyableButton button) {
        int effectiveIndex = index - page * perPage;
        final int currentPage = page;
        if (effectiveIndex >= 0 && effectiveIndex < perPage) {
            int rowIndex = Math.min(rows.length - 1, effectiveIndex / ROW_LIMIT);
            JPanel row = rows[rowIndex];
            row.add(button);
            Character hotkey = POSSIBLE_HOTKEYS.get(effectiveIndex);
            register(hotkey, button);
            if (DEFAULT_CHOICES.contains(button.getText()) && !hotkeyMapping.containsKey(' ')) {
                hotkeyMapping.put(' ', button); 
            }
        } else if (effectiveIndex == -1) {
            KeyableButton leftPage = new RunnableButton("<<<", new Runnable() {
				@Override
				public void run() {
					setPage(currentPage - 1);
				}
			});
            rows[0].add(leftPage, 0);
            register('~', leftPage);
        } else if (effectiveIndex == perPage){
            KeyableButton rightPage = new RunnableButton(">>>", new Runnable() {
				@Override
				public void run() {
					setPage(currentPage + 1);
				}
			});
            rows[0].add(rightPage);
            register('`', rightPage);
        }
        index += 1;
    }

    public void setPage(int page) {
        this.page = page;
        clear();
        for(KeyableButton b: buttons){
        	use(b);
        }
        refresh();
    }

    public Optional<KeyableButton> getButtonForHotkey(char keyChar) {
        return Optional.ofNullable(hotkeyMapping.get(keyChar));
    }

    public void register(Character hotkey, KeyableButton button) {
        button.setHotkeyTextTo(hotkey.toString().toUpperCase());
        hotkeyMapping.put(hotkey, button);
    }

/*	public void setBackground(Color frameColor) {
		setBackground(frameColor);
	}*/
}